This is a program that lists files/directories present in a directory like "ls" command.

=> Its default committed version performs just "ls -li" but
=> If you want to do a simple listing of files in alphabetically sorted columns then you can comment the startup code in main and uncomment previously commented code. It is very simple as I have written comments before both sections.