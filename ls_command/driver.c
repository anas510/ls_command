#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

extern int errno;
extern char** environ;
short col;
int max;			//stores length of longest fileName
int avgWidth;

short columns();
int cmpStr(const void* , const void* );
void listDirContents(DIR* );
void longListDirContents(char*);
int countEntries(DIR* );			//count number of files in directory
int findLinesCount(char**,int);
char* calculatePermissions(struct stat*);
char* userName(struct stat*);
char* groupName(struct stat*);
char* getMTime();

int main(int argc,char* argv[])
{
	col=columns();

	//For version-1,2
	DIR* d;
	if(argc<2){	//display contents of PWD
		d=opendir(getenv("PWD"));
		listDirContents(d);
	}
	else{
		int i=1;
		while(i<argc)
		{
			if(argc>2)
				printf("%s:\n",argv[i]);
			errno=0;
			d=opendir(argv[i]);
			if(d==NULL && errno!=0){	//error
				printf("\nError opening file %s",argv[i]);
				perror("");			
				i++;
				continue;
			}
			listDirContents(d);		
			if(i!=argc-1)
				printf("\n");
			closedir(d);
			i++;	
		}
	}
	// For version-3
	// if(argc==1){	//display contents of PWD
	// 	longListDirContents(getenv("PWD"));
	// }
	// else{
	// 	int i=1;
	// 	while(i<argc)
	// 	{
	// 		if(argc>2)
	// 		{
	// 			if(i!=1)
	// 				printf("\n");
	// 			printf("%s:\n",argv[i]);
	// 		}
			
	// 		longListDirContents(argv[i]);		
	// 		i++;	
	// 	}
	// }

	return 0;
}

short columns()
{
	struct winsize w;
	ioctl(STDIN_FILENO, TIOCGWINSZ, &w);
	return w.ws_col;
}

int cmpStr(const void* s1, const void* s2)
{
	return strcasecmp(*((const char**)s1),*((const char**)s2));	
}

void listDirContents(DIR* dp)		//For version 1,2
{
	struct dirent * entry;
	errno=0;
	
	int fileCount=countEntries(dp);
	int i=0;
	char** files=(char**)malloc(sizeof(char*)*fileCount);
	while((entry = readdir(dp))!=NULL)
	{
		if(entry->d_name[0]=='.')			//don't display hidden file
		{
			errno=0;
			continue;
		}
		files[i++]=entry->d_name;
		errno=0;
	}
	if (entry == NULL && errno != 0) 	//Error
	{	
		perror("Error reading directory");
		exit(0);
	} 
	//File completed

	//If dir contain any entry other than . & ..
	if(fileCount>0){
		
		if(fileCount>1){
			qsort(files,fileCount,sizeof(char*),cmpStr);
		}
		
		//To calculate max possible columns and exact linesCount
		int lines=findLinesCount(files,fileCount);

		// printf("c=%d\tcolumns=%d\tavgWidth=%d\t max=%d\n",c,col,avgWidth,max);
		
		int indent=0;
		int l=0;		//store number of lines printed for current column
		//to calculateMax
		int max=0;
		int colMaxCount=0;
		//finds max column width (only for 1st column)
		for(;colMaxCount<fileCount && colMaxCount < lines;colMaxCount++)
		{
			int len=strlen(files[colMaxCount]);
			if(len > max)
				max=len;
		}
		max+=2;

		//To print fileNames on stdout
		for(int i=0;i<fileCount;i++){
			if(indent>0)
				printf("\033[%dC",indent);
			printf("%s\n",files[i]);
			l++;
			
			if(lines == l && i<fileCount-1)		//column completed
			{
				printf("\033[%dA",l);			//as each name also prints \n
				l=0;
				indent+=max;
				max=0;
				//finds max column width (only for next column)
				for(int j=0;colMaxCount<fileCount && j < lines;colMaxCount++,j++)
				{
					int len=strlen(files[colMaxCount]);
					if(len>max)
						max=len;
				}
				max+=2;
			}
		}
		
		if(l!=0 && lines != l)
		{
			int i=0;
			while(i++<lines-l)
				printf("\n");
		}

		//The returned fileNames are not on heap so no need to delete them
		//if we free() them error occurs
		free(files);
	}	
}

void longListDirContents(char* f)
{
	DIR* dp;
	errno=0;
	dp=opendir(f);
	if(dp==NULL && errno!=0){	//error
		printf("\nError opening file %s",f);
		perror("");			
		exit(0);
	}

	struct dirent * entry;
	errno=0;
	
	int fileCount=countEntries(dp);
	int i=0;
	char** files=(char**)malloc(sizeof(char*)*fileCount);
	while((entry = readdir(dp))!=NULL)
	{
		if(entry->d_name[0]=='.')			//don't display hidden file
		{
			errno=0;
			continue;
		}
		files[i++]=entry->d_name;
		errno=0;
	}
	if (entry == NULL && errno != 0) 	//Error
	{	
		perror("Error reading directory");
		exit(0);
	} 
	//File completed
	if(fileCount>0){
		
		if(fileCount>1){
			qsort(files,fileCount,sizeof(char*),cmpStr);
		}
		
		//Without this strcat will change original argv[] in main and multifile won;t work
		char tF[strlen(f)+2];		
		strcpy(tF,f);

		char* path=strcat(tF,"/");
				
		for(int i=0;i<fileCount;i++)
		{
			struct stat info;
			
			char file[256];
			strcpy(file,path);			
			strcat(file,files[i]);

			int s=lstat(file, &info);
			if(s==0)
			{
				char* perm=calculatePermissions(&info);
				char* user=userName(&info);
				char* group=groupName(&info);				
				char* time=getMTime(info.st_mtime);
				printf("%8ld %s %3ld %-10s%-10s %10ld %s ",info.st_ino,perm,info.st_nlink,user,group,info.st_size,time);
				//for printing file name with proper color
				printf("\033[1m");
				if(perm[0]=='-')
					printf("%s",files[i]);
				else if(perm[0]=='d')
					printf("\033[34m%s\033[39m",files[i]);
				else if(perm[0]=='c' || perm[0]=='b')
					printf("\033[33m%s\033[39m",files[i]);
				else if(perm[0]=='p')
					printf("\033[33m\033[42m%s\033[39m\033[42m",files[i]);
				else if(perm[0]=='l')		// || info.st_nlink>0
				{
					printf("\033[36m%s\033[39m\033[0m",files[i]);
					char* realFile=realpath(file,NULL);
					printf(" -> %s",realFile);
					free(realFile);
				}
				printf("\033[0m");
				free(perm);
				free(time);
			}
			else if(s==-1){
				perror("Error getting permissions");
				exit(0);
			}
			if(i!=fileCount)
				printf("\n");
		}
		free(files);
	}
	closedir(dp);
}

char* getMTime(long int time)
{
	char* t=ctime(&time);
	char* timeStr=(char*)malloc(sizeof(char)*12);
	char* tok=strtok(t," ");

	int i=1;
	char* timeTok;
	char* mtime=(char*)malloc(sizeof(char)*8);
	// strcat(mtime," ");
	// mtime[0]='\0';
	while( tok != NULL ) 
   {
   		// printf("\n\n%s",tok);
      	if(i==2)
		{
			strcpy(timeStr,"");
			strcat(timeStr,tok);
			
			timeStr[3]=' ';
			timeStr[4]='\0';
		}
		if(i==3)
		{
			int day=atoi(tok);
			
			if(day<=9)
				strcat(timeStr," ");
			
			strcat(timeStr,tok);			
		}
		if(i==4)
		{
			strcpy(mtime," ");
			int j=1;

			timeTok=strtok(tok,":");
			while( timeTok != NULL ) 
			{
				
				if(j==1 || j==2)
				{
					strcat(mtime,timeTok);
					if(j==1)
						strcat(mtime,":");					
				}
				timeTok=strtok(NULL,":");
				j++;
			}
			strcat(timeStr,mtime);
			//printf("\n\n%s",timeStr);			
		}
    
		tok = strtok(NULL, " ");
		i++;
   }
   
   return timeStr;
}

char* calculatePermissions(struct stat* info)
{
	int type=info->st_mode & 0170000;
	char* perm=(char*)malloc(sizeof(char)*11);
	strcpy(perm,"----------");
	//For fileType
	if (type == 0010000)
		perm[0]='p';
	else if (type == 0020000)
		perm[0]='c';
	else if (type == 0040000)
		perm[0]='d';
	else if (type == 0060000)
		perm[0]='b';
	else if (type == 0100000)
		perm[0]='-';
	else if (type == 0120000)
		perm[0]='l';
	else if (type == 0140000)
		perm[0]='s';
	//For user permissions
	if((info->st_mode & 0000400) == 0000400)
		perm[1]='r';
	if((info->st_mode & 0000200) == 0000200)
		perm[2]='w';
	if((info->st_mode & 0000100) == 0000100)
		perm[3]='x';
	//For group permissions
	if((info->st_mode & 0000040) == 0000040)
		perm[4]='r';
	if((info->st_mode & 0000020) == 0000020)
		perm[5]='w';
	if((info->st_mode & 0000010) == 0000010)
		perm[6]='x';
	//For others permissions
	if((info->st_mode & 0000004) == 0000004)
		perm[7]='r';
	if((info->st_mode & 0000002) == 0000002)
		perm[8]='w';
	if((info->st_mode & 0000001) == 0000001)
		perm[9]='x';
	
	//For Special permisiions
	if((info->st_mode & 0004000) == 0004000)
	{
		if(perm[3]=='x')
			perm[3]='S';
		else
			perm[3]='s';
	}
	if((info->st_mode & 0002000) == 0002000)
	{
		if(perm[6]=='x')
			perm[6]='S';
		else
			perm[6]='s';
	}
	if((info->st_mode & 0001000) == 0001000)
	{
		if(perm[9]=='x')
			perm[9]='T';
		else
			perm[9]='t';
	}
	return perm;
}

char* userName(struct stat* info)
{
	errno = 0;
	struct passwd * pwd = getpwuid(info->st_uid);
	if (pwd == NULL){
		if (errno == 0)
			fprintf(stderr,"Record not found in passwd file.\n");
		else
			perror("getpwuid failed");
	}
	
	char* name=pwd->pw_name;
	return name;
}

char* groupName(struct stat* info)
{
	errno = 0;
	struct group * grp = getgrgid(info->st_gid);
	if (grp == NULL){
		if (errno == 0)
			fprintf(stderr,"Record not found in passwd file.\n");
		else
			perror("getgrgid failed");
	}
	char* name=grp->gr_name;
	return name;
}

int countEntries(DIR* dp){
	int count=0;
	struct dirent * entry;
	max=0;
	avgWidth=0;
	while((entry = readdir(dp))!=NULL)
	{
		if(entry->d_name[0]=='.')			//don't count hidden files
		{
			errno=0;
			continue;
		}
		int t=strlen(entry->d_name);
		avgWidth+=t;
		if(t > max)
			max=t;
		count++;
		errno=0;
	}
	
	if (entry == NULL && errno != 0) 	//Error
	{	
		perror("Error reading directory");
		exit(0);
	}
	else{
		rewinddir(dp);			//rwind directory so that all these files can be displayed
	}

	avgWidth/=count;
	//increase avg width by ratio of max to avgWidth
	if(col>110)
		avgWidth*=1.85;
	else if(col<35)
		avgWidth=max;
	else if(count>35)
	{
		if(avgWidth< .6*max)
			avgWidth=max*.85;
		else if(avgWidth>=.75*max)
			avgWidth=max;
	}
	else 
		avgWidth*=(max/avgWidth);
		
	return count;
}

int findLinesCount(char** files,int count)
{
	int max=0;
	
	int linesCount=0;
	int prevColWidth=0;
	int totalColsWidth=0;

	for(int l=1;l<=count;l++)			//max lines can be equal to files for small terminalSize
	{
		int colMax=0;
		max=0;
		for(int i=0;i<count;i++)
		{
			int len=strlen(files[i]);
			if(len>max)
				max=len;
			if((i+1)%l==0 || i==count-1)
			{
				colMax=max+2;
				totalColsWidth+=colMax;
				max=0;
				colMax=0;
			}
			if(totalColsWidth >= col)		//if width of columns becomes larger them terminalWidth increase lines count
			{
				max=0;
				colMax=0;
				totalColsWidth=0;
				break;
			}
		}

		if(totalColsWidth!=0)		//If it is not exxcedding current terminal Width save this
		{
			if(totalColsWidth>prevColWidth)
			{
				prevColWidth=totalColsWidth;
				linesCount=l;
			}
			totalColsWidth=0;
			//printf("line%d\tprevColWidth=%d\n\n",linesCount,prevColWidth);
		}
		else if(prevColWidth!=0)		//Width exceeds terminal columns so previous iteration has best result
			break;
	}

	return linesCount;
}